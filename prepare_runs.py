def create_test_configs(vu, test_type, test_folder):

    tb_lib = vu.add_library("tb_lib")
    tb_lib.add_source_files("tests/*.vhd")
    tb_lib.add_source_files("tests/" + test_folder + "*.vhd")

    TEST_CONFIG_YAML_PATH = 'tests/test_config.yml'
    
    with open(TEST_CONFIG_YAML_PATH, "r") as read_file:
        yaml_objects = yaml.load_all(read_file, Loader=yaml.FullLoader)    
        testbenches = []
       
        for yaml_object in yaml_objects:
            for yaml_name, yaml_params in yaml_object.items():
                
                if yaml_name.lower() not in (tb.name for tb in tb_lib.get_test_benches()):
                    continue
                
                testbench_name = yaml_name
                config_params  = yaml_params
                
                assert config_params["test type"] == test_type, "Wrong type of test!"
                
                testbenches.append(tb_lib.entity(testbench_name))
                testbench = testbenches[-1]
                
                # Check requirements integrity for items
                for item in config_params["req coverage"]:
                    text = "Requirement error in " + testbench_name + " for " + item
                    assert set(config_params["req coverage"][item]).issubset(requirements[item]), text
                
                # Check requirements integrity for simulation models
                text = "Requirement error in " + testbench_name + " for simulation models"
                assert set(config_params["sim coverage"]).issubset(simulation_models), text
                
                attributes = {}
                for attr in ("id", "description", "req coverage", "sim coverage"):
                    attributes["." + attr] = config_params[attr]
                
                generics = config_params["generics"]
                all_srs = []
                
                if "P12345678" in config_params["req coverage"]:
                    for srs in config_params["req coverage"]["P12345678"]:
                        all_srs.append(str(int(srs[-4:])))
                    if len(all_srs) > 0:
                        generics["REQS_LIST"] = ','.join(all_srs)
                
                testbench.add_config(
                    name=config_params["name"],
                    generics=generics,
                    attributes=attributes
                )

